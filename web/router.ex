defmodule Enquete.Router do
  use Enquete.Web, :router

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_flash
    plug :protect_from_forgery
    plug :put_secure_browser_headers
  end

  pipeline :api do
    plug :accepts, ["json"]
  end

  scope "/", Enquete do
    pipe_through :browser # Use the default browser stack

    get "/", PageController, :index

    resources "/enquetes", EnqueteController do
      resources "/answers", AnswerController
    end

  end

  # Other scopes may use custom stacks.
  # scope "/api", Enquete do
  #   pipe_through :api
  # end


end
