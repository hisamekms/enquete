defmodule Enquete.Answer do
  use Enquete.Web, :model

  schema "answers" do
    field :answerer, :string
    belongs_to :enquete, Enquete.Enquete
    has_many :details, Enquete.AnswerDetail, on_delete: :delete_all

    timestamps
  end

  @required_fields ~w(answerer enquete_id)
  @optional_fields ~w()

  @doc """
  Creates a changeset based on the `model` and `params`.

  If no params are provided, an invalid changeset is returned
  with no validation performed.
  """
  def changeset(model, params \\ :empty) do
    model
    |> cast(params, @required_fields, @optional_fields)
  end
end
