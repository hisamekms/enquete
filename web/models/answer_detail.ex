defmodule Enquete.AnswerDetail do
  use Enquete.Web, :model

  schema "answer_details" do
    field :no, :integer
    field :answer_string, :string
    belongs_to :answer, Enquete.Answer

    timestamps
  end

  @required_fields ~w(no answer_id)
  @optional_fields ~w(answer_string)

  @doc """
  Creates a changeset based on the `model` and `params`.

  If no params are provided, an invalid changeset is returned
  with no validation performed.
  """
  def changeset(model, params \\ :empty) do
    model
    |> cast(params, @required_fields, @optional_fields)
  end
end
