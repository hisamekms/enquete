defmodule Enquete.AnswerView do
  use Enquete.Web, :view

  require Logger

  def answer_string(answer, index) do
    # TODO パターンマッチとかにできるよね？？
    if Ecto.assoc_loaded?(answer.details) do
      if length(answer.details) >= (index + 1) do
        str = Enum.at(answer.details, index).answer_string
        if str do
          str
        else
          ""
        end
      else
        ""
      end
    else
      ""
    end
  end
end
