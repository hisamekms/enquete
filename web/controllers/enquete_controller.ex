defmodule Enquete.EnqueteController do
  use Enquete.Web, :controller

  alias Enquete.Enquete

  plug :scrub_params, "enquete" when action in [:create, :update]

  def index(conn, _params) do
    enquetes = Repo.all(Enquete)
    render(conn, "index.html", enquetes: enquetes)
  end

  def new(conn, _params) do
    changeset = Enquete.changeset(%Enquete{})
    render(conn, "new.html", changeset: changeset)
  end

  def create(conn, %{"enquete" => enquete_params}) do
    changeset = Enquete.changeset(%Enquete{}, enquete_params)

    case Repo.insert(changeset) do
      {:ok, _enquete} ->
        conn
        |> put_flash(:info, "Enquete created successfully.")
        |> redirect(to: enquete_path(conn, :index))
      {:error, changeset} ->
        render(conn, "new.html", changeset: changeset)
    end
  end

  def show(conn, %{"id" => id}) do
    enquete = Repo.get!(Enquete, id)
    render(conn, "show.html", enquete: enquete)
  end

  def edit(conn, %{"id" => id}) do
    enquete = Repo.get!(Enquete, id)
    changeset = Enquete.changeset(enquete)
    render(conn, "edit.html", enquete: enquete, changeset: changeset)
  end

  def update(conn, %{"id" => id, "enquete" => enquete_params}) do
    enquete = Repo.get!(Enquete, id)
    changeset = Enquete.changeset(enquete, enquete_params)

    case Repo.update(changeset) do
      {:ok, enquete} ->
        conn
        |> put_flash(:info, "Enquete updated successfully.")
        |> redirect(to: enquete_path(conn, :show, enquete))
      {:error, changeset} ->
        render(conn, "edit.html", enquete: enquete, changeset: changeset)
    end
  end

  def delete(conn, %{"id" => id}) do
    enquete = Repo.get!(Enquete, id)

    # Here we use delete! (with a bang) because we expect
    # it to always work (and if it does not, it will raise).
    Repo.delete!(enquete)

    conn
    |> put_flash(:info, "Enquete deleted successfully.")
    |> redirect(to: enquete_path(conn, :index))
  end
end
