defmodule Enquete.AnswerController do
  use Enquete.Web, :controller
  require Logger

  alias Enquete.Answer
  alias Enquete.AnswerDetail

  plug :scrub_params, "answer" when action in [:create, :update]

  def index(conn, %{"enquete_id" => enquete_id}) do
    details_query = from ad in AnswerDetail, order_by: ad.no
    answers = Repo.all(
      from a in Answer,
        where: a.enquete_id == ^enquete_id,
      select: a,
      preload: [details: ^details_query]
    )
    render(conn, "index.html", answers: answers, enquete_id: enquete_id)
  end

  def new(conn, %{"enquete_id" => enquete_id}) do
    changeset = Answer.changeset(%Answer{})
    render(conn, "new.html", changeset: changeset, enquete_id: enquete_id)
  end

  def create(conn, %{"answer" => answer_params}) do
    changeset = Answer.changeset(%Answer{}, %{
        enquete_id: answer_params["enquete_id"],
        answerer: answer_params["answerer"]
      })

    case Repo.transaction(fn ->
      _answer = Repo.insert!(changeset)

      # TODO なんかいい感じに...
      Repo.insert!(AnswerDetail.changeset(%AnswerDetail{}, %{
          answer_id: _answer.id,
          no: 1,
          answer_string: answer_params["answer_string0"]
        }))

      Repo.insert!(AnswerDetail.changeset(%AnswerDetail{}, %{
          answer_id: _answer.id,
          no: 2,
          answer_string: answer_params["answer_string1"]
        }))

      Repo.insert!(AnswerDetail.changeset(%AnswerDetail{}, %{
          answer_id: _answer.id,
          no: 3,
          answer_string: answer_params["answer_string2"]
        }))

      Repo.insert!(AnswerDetail.changeset(%AnswerDetail{}, %{
          answer_id: _answer.id,
          no: 4,
          answer_string: answer_params["answer_string3"]
        }))

      Repo.insert!(AnswerDetail.changeset(%AnswerDetail{}, %{
          answer_id: _answer.id,
          no: 5,
          answer_string: answer_params["answer_string4"]
        }))

      _answer
    end) do
      {:ok, _answer} ->
        conn
        |> put_flash(:info, "Answer created successfully.")
        |> redirect(to: enquete_answer_path(conn, :index, answer_params["enquete_id"]))
      {:error, changeset} ->
        render(conn, "new.html", changeset: changeset, enquete_id: changeset["enquete_id"])
    end
  end

  def show(conn, %{"id" => id}) do
    answer = Repo.get!(Answer, id)
    render(conn, "show.html", answer: answer)
  end

  def edit(conn, %{"id" => id}) do
    answer = Repo.get!(Answer, id) |> Repo.preload(:details)
    changeset = Answer.changeset(answer)

    render(conn, "edit.html", answer: answer, changeset: changeset, enquete_id: answer.enquete_id)
  end

  def update(conn, %{"id" => id, "answer" => answer_params}) do
    details_query = from ad in AnswerDetail, order_by: ad.no
    answer = Repo.get!(Answer, id) |> Repo.preload([details: details_query])

    changeset = Answer.changeset(answer, %{
        enquete_id: answer_params["enquete_id"],
        answerer: answer_params["answerer"]
      })

    case Repo.transaction(fn ->
      _answer = Repo.update!(changeset)

      # TODO なんかいい感じに...
      Repo.update!(AnswerDetail.changeset(Enum.at(answer.details, 0), %{
          answer_string: answer_params["answer_string0"]
        }))

      Repo.update!(AnswerDetail.changeset(Enum.at(answer.details, 1), %{
          answer_string: answer_params["answer_string1"]
        }))

      Repo.update!(AnswerDetail.changeset(Enum.at(answer.details, 2), %{
          answer_string: answer_params["answer_string2"]
        }))

      Repo.update!(AnswerDetail.changeset(Enum.at(answer.details, 3), %{
          answer_string: answer_params["answer_string3"]
        }))

      Repo.update!(AnswerDetail.changeset(Enum.at(answer.details, 4), %{
          answer_string: answer_params["answer_string4"]
        }))

      _answer
    end) do
      {:ok, _answer} ->
        conn
        |> put_flash(:info, "Answer updated successfully.")
        |> redirect(to: enquete_answer_path(conn, :show, answer.enquete_id, answer))
      {:error, changeset} ->
        render(conn, "edit.html", answer: answer, changeset: changeset, enquete_id: answer.enquete_id)
    end
  end

  def delete(conn, %{"id" => id}) do
    answer = Repo.get!(Answer, id)

    # Here we use delete! (with a bang) because we expect
    # it to always work (and if it does not, it will raise).
    Repo.delete!(answer)

    conn
    |> put_flash(:info, "Answer deleted successfully.")
    |> redirect(to: enquete_answer_path(conn, :index, answer.enquete_id))
  end
end
