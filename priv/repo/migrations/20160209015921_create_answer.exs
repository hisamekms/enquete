defmodule Enquete.Repo.Migrations.CreateAnswer do
  use Ecto.Migration

  def change do
    create table(:answers) do
      add :answerer, :string
      add :enquete_id, references(:enquetes, on_delete: :nothing)

      timestamps
    end
    create index(:answers, [:enquete_id])

  end
end
