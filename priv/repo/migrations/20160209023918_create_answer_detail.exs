defmodule Enquete.Repo.Migrations.CreateAnswerDetail do
  use Ecto.Migration

  def change do
    create table(:answer_details) do
      add :no, :integer
      add :answer_string, :string
      add :answer_id, references(:answers, on_delete: :nothing)

      timestamps
    end
    create index(:answer_details, [:answer_id])

  end
end
