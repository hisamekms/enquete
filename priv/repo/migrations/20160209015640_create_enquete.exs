defmodule Enquete.Repo.Migrations.CreateEnquete do
  use Ecto.Migration

  def change do
    create table(:enquetes) do
      add :title, :string

      timestamps
    end

  end
end
