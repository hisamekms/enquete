defmodule Enquete.EnqueteTest do
  use Enquete.ModelCase

  alias Enquete.Enquete

  @valid_attrs %{title: "some content"}
  @invalid_attrs %{}

  test "changeset with valid attributes" do
    changeset = Enquete.changeset(%Enquete{}, @valid_attrs)
    assert changeset.valid?
  end

  test "changeset with invalid attributes" do
    changeset = Enquete.changeset(%Enquete{}, @invalid_attrs)
    refute changeset.valid?
  end
end
