defmodule Enquete.AnswerDetailTest do
  use Enquete.ModelCase

  alias Enquete.AnswerDetail

  @valid_attrs %{answer: "some content", no: 42}
  @invalid_attrs %{}

  test "changeset with valid attributes" do
    changeset = AnswerDetail.changeset(%AnswerDetail{}, @valid_attrs)
    assert changeset.valid?
  end

  test "changeset with invalid attributes" do
    changeset = AnswerDetail.changeset(%AnswerDetail{}, @invalid_attrs)
    refute changeset.valid?
  end
end
