defmodule Enquete.EnqueteControllerTest do
  use Enquete.ConnCase

  alias Enquete.Enquete
  @valid_attrs %{title: "some content"}
  @invalid_attrs %{}

  test "lists all entries on index", %{conn: conn} do
    conn = get conn, enquete_path(conn, :index)
    assert html_response(conn, 200) =~ "Listing enquetes"
  end

  test "renders form for new resources", %{conn: conn} do
    conn = get conn, enquete_path(conn, :new)
    assert html_response(conn, 200) =~ "New enquete"
  end

  test "creates resource and redirects when data is valid", %{conn: conn} do
    conn = post conn, enquete_path(conn, :create), enquete: @valid_attrs
    assert redirected_to(conn) == enquete_path(conn, :index)
    assert Repo.get_by(Enquete, @valid_attrs)
  end

  test "does not create resource and renders errors when data is invalid", %{conn: conn} do
    conn = post conn, enquete_path(conn, :create), enquete: @invalid_attrs
    assert html_response(conn, 200) =~ "New enquete"
  end

  test "shows chosen resource", %{conn: conn} do
    enquete = Repo.insert! %Enquete{}
    conn = get conn, enquete_path(conn, :show, enquete)
    assert html_response(conn, 200) =~ "Show enquete"
  end

  test "renders page not found when id is nonexistent", %{conn: conn} do
    assert_error_sent 404, fn ->
      get conn, enquete_path(conn, :show, -1)
    end
  end

  test "renders form for editing chosen resource", %{conn: conn} do
    enquete = Repo.insert! %Enquete{}
    conn = get conn, enquete_path(conn, :edit, enquete)
    assert html_response(conn, 200) =~ "Edit enquete"
  end

  test "updates chosen resource and redirects when data is valid", %{conn: conn} do
    enquete = Repo.insert! %Enquete{}
    conn = put conn, enquete_path(conn, :update, enquete), enquete: @valid_attrs
    assert redirected_to(conn) == enquete_path(conn, :show, enquete)
    assert Repo.get_by(Enquete, @valid_attrs)
  end

  test "does not update chosen resource and renders errors when data is invalid", %{conn: conn} do
    enquete = Repo.insert! %Enquete{}
    conn = put conn, enquete_path(conn, :update, enquete), enquete: @invalid_attrs
    assert html_response(conn, 200) =~ "Edit enquete"
  end

  test "deletes chosen resource", %{conn: conn} do
    enquete = Repo.insert! %Enquete{}
    conn = delete conn, enquete_path(conn, :delete, enquete)
    assert redirected_to(conn) == enquete_path(conn, :index)
    refute Repo.get(Enquete, enquete.id)
  end
end
