ExUnit.start

Mix.Task.run "ecto.create", ~w(-r Enquete.Repo --quiet)
Mix.Task.run "ecto.migrate", ~w(-r Enquete.Repo --quiet)
Ecto.Adapters.SQL.begin_test_transaction(Enquete.Repo)

