#!/bin/bash
# npm install
export ENQUETE_DB_HOST=${DB_PORT_3306_TCP_ADDR}
export ENQUETE_DB_PORT=${DB_PORT_3306_TCP_PORT}
# export FOO=TESTTEST
# export BAR=${DB_PORT}

mix compile
mix ecto.create
mix ecto.migrate
mix phoenix.server
